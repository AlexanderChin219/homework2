package gui;

import apptemplate.AppTemplate;
import components.AppWorkspaceComponent;
import controller.HangmanController;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ToolBar;
import javafx.scene.layout.*;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;
import org.w3c.dom.css.Rect;
import propertymanager.PropertyManager;
import ui.AppGUI;

import java.awt.*;
import java.io.IOException;
import java.util.Stack;
import javafx.scene.paint.Color;
import static hangman.HangmanProperties.*;

/**
 * This class serves as the GUI component for the Hangman game.
 *
 * @author Ritwik Banerjee
 */
public class Workspace extends AppWorkspaceComponent {

    AppTemplate app; // the actual application
    AppGUI      gui; // the GUI inside which the application sits

    Label             guiHeadingLabel;   // workspace (GUI) heading label
    HBox              headPane;          // conatainer to display the heading
    HBox              bodyPane;          // container for the main game displays
    ToolBar           footToolbar;       // toolbar for game buttons
    BorderPane        figurePane;        // container to display the namesake graphic of the (potentially) hanging person
    VBox              gameTextsPane;     // container to display the text-related parts of the game
    HBox              guessedLetters;    // text area displaying all the letters guessed so far
    HBox              remainingGuessBox; // container to display the number of remaining guesses

    Button            startGame;         // the button to start playing a game of Hangman
    HangmanController controller;


    HBox              guesses; //text area displaying all inccorrect guessed letters so far
    HBox              hintArea; //hbox for hint button

    VBox              hangmanArea;
    Shape[]           hangmanArray;
    Rectangle         bottom;
    Rectangle         middle;
    Rectangle         top;
    Rectangle         hanger;
    Circle            head;
    Rectangle         body;
    Rectangle         leftArm;
    Rectangle         rightArm;
    Rectangle         leftLeg;
    Rectangle         rightLeg;







    /**
     * Constructor for initializing the workspace, note that this constructor
     * will fully setup the workspace user interface for use.
     *
     * @param initApp The application this workspace is part of.
     * @throws IOException Thrown should there be an error loading application
     *                     data for setting up the user interface.
     */
    public Workspace(AppTemplate initApp) throws IOException {
        app = initApp;
        gui = app.getGUI();
        controller = (HangmanController) gui.getFileController();    //new HangmanController(app, startGame); <-- THIS WAS A MAJOR BUG!??
        layoutGUI();     // initialize all the workspace (GUI) components including the containers and their layout
        setupHandlers(); // ... and set up event handling
    }

    private void layoutGUI() {
        PropertyManager propertyManager = PropertyManager.getManager();
        guiHeadingLabel = new Label(propertyManager.getPropertyValue(WORKSPACE_HEADING_LABEL));

        headPane = new HBox();
        headPane.getChildren().add(guiHeadingLabel);
        headPane.setAlignment(Pos.CENTER);

        figurePane = new BorderPane();
        guessedLetters = new HBox();
        guessedLetters.setStyle("-fx-background-color: transparent;");
        remainingGuessBox = new HBox();
        gameTextsPane = new VBox();
        guesses = new HBox(); //possible guesses
        hintArea = new HBox();
        hangmanArea = new VBox();
        Rectangle x = new Rectangle(10,10, Color.PURPLE);


        setUpHangman();

//        figurePane.getChildren().addAll(hangmanArea);




        gameTextsPane.getChildren().setAll(remainingGuessBox, guessedLetters, guesses, hintArea,hangmanArea);//adds all the nodes to workspace


        bodyPane = new HBox();
        bodyPane.getChildren().addAll(gameTextsPane);
        bodyPane.setPrefHeight(700);


        startGame = new Button("Start Playing"); //create button
        HBox blankBoxLeft  = new HBox();
        HBox blankBoxRight = new HBox();
        HBox.setHgrow(blankBoxLeft, Priority.ALWAYS);
        HBox.setHgrow(blankBoxRight, Priority.ALWAYS);
        footToolbar = new ToolBar(blankBoxLeft, startGame, blankBoxRight); //center button
        figurePane.setBottom(footToolbar); //change



        workspace = new VBox();
        workspace.getChildren().addAll(headPane, bodyPane, hangmanArea, footToolbar);
    }


    public void setUpHangman(){
        hangmanArray = new Shape[10];

        //hangman graphics
        bottom = new Rectangle(380,350,380,5);

        middle = new Rectangle(760, 105, 5, 250);

        top = new Rectangle(550,105,210,5);

        hanger = new Rectangle(550,105,5,50);

        head = new Circle(550,170,35);

        body = new Rectangle(550,200,5,90);

        rightArm = new Rectangle(550,220, 50,5);
        leftArm = new Rectangle(505, 220, 50,5);
        rightLeg = new Rectangle(550,298,50,5 );
        leftLeg = new Rectangle(505,298,50,5);


        //angle arms and legs
        rightArm.setRotate(30);
        leftArm.setRotate(150);
        leftLeg.setRotate(150);
        rightLeg.setRotate(30);

        //store in array
        hangmanArray[0] = bottom;
        hangmanArray[1] = middle;
        hangmanArray[2] = top;
        hangmanArray[3] = hanger;
        hangmanArray[4] = head;
        hangmanArray[5] = body;
        hangmanArray[6] = rightArm;
        hangmanArray[7] = leftArm;
        hangmanArray[8] = rightLeg;
        hangmanArray[9] = leftLeg;

        for(int i =0; i<hangmanArray.length; i++){
            figurePane.getChildren().add(hangmanArray[i]);
            hangmanArray[i].setVisible(false);
        }
        hangmanArea.getChildren().addAll(figurePane);

    }

    private void setupHandlers() {
        startGame.setOnMouseClicked(e -> controller.start());
    }

    /**
     * This function specifies the CSS for all the UI components known at the time the workspace is initially
     * constructed. Components added and/or removed dynamically as the application runs need to be set up separately.
     */
    @Override
    public void initStyle() {
        PropertyManager propertyManager = PropertyManager.getManager();

        gui.getAppPane().setId(propertyManager.getPropertyValue(ROOT_BORDERPANE_ID));
        gui.getToolbarPane().getStyleClass().setAll(propertyManager.getPropertyValue(SEGMENTED_BUTTON_BAR));
        gui.getToolbarPane().setId(propertyManager.getPropertyValue(TOP_TOOLBAR_ID));

        ObservableList<Node> toolbarChildren = gui.getToolbarPane().getChildren();
        toolbarChildren.get(0).getStyleClass().add(propertyManager.getPropertyValue(FIRST_TOOLBAR_BUTTON));
        toolbarChildren.get(toolbarChildren.size() - 1).getStyleClass().add(propertyManager.getPropertyValue(LAST_TOOLBAR_BUTTON));

        workspace.getStyleClass().add(CLASS_BORDERED_PANE);
        guiHeadingLabel.getStyleClass().setAll(propertyManager.getPropertyValue(HEADING_LABEL));

    }

    /** This function reloads the entire workspace */
    @Override
    public void reloadWorkspace() {
        /* does nothing; use reinitialize() instead */
    }

    public VBox getGameTextsPane() {
        return gameTextsPane;
    }

    public HBox getRemainingGuessBox() {
        return remainingGuessBox;
    }


    public HBox getGuesses(){ return guesses;}



    public HBox getHintArea() {return hintArea;}

    public Button getStartGame() {
        return startGame;
    }

    public void setVisHangman(int guessNum){
        hangmanArray[guessNum].setVisible(true);
        }

    public void reinitialize() {
        guessedLetters = new HBox();
        guessedLetters.setStyle("-fx-background-color: transparent;");
        remainingGuessBox = new HBox();
        guesses = new HBox();
        gameTextsPane = new VBox();
        hintArea = new HBox();
        for(int i = 0; i <hangmanArray.length; i++){
            hangmanArray[i].setVisible(false);
        }

        gameTextsPane.getChildren().setAll(remainingGuessBox, guessedLetters, guesses,hintArea);
        bodyPane.getChildren().setAll(figurePane, gameTextsPane);



    }
}
