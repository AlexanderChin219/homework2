package controller;

import apptemplate.AppTemplate;
import data.GameData;
import gui.Workspace;
import javafx.animation.AnimationTimer;
import javafx.application.Platform;
import javafx.geometry.*;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import propertymanager.PropertyManager;
import ui.AppMessageDialogSingleton;
import ui.YesNoCancelDialogSingleton;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Stack;

import javafx.scene.paint.Color;

import static settings.AppPropertyType.*;
import static settings.InitializationParameters.APP_WORKDIR_PATH;

/**
 * @author Ritwik Banerjee
 */
public class HangmanController implements FileController {

    public enum GameState {
        UNINITIALIZED,
        INITIALIZED_UNMODIFIED,
        INITIALIZED_MODIFIED,
        ENDED
    }

    private AppTemplate appTemplate; // shared reference to the application
    private GameData    gamedata;    // shared reference to the game being played, loaded or saved
    private GameState   gamestate;   // the state of the game being shown in the workspace
    private Text[]      progress;    // reference to the text area for the word
    private boolean     success;     // whether or not player was successful
    private int         discovered;  // the number of letters already discovered
    private Button      gameButton;  // shared reference to the "start game" button
    private Label       remains;     // dynamically updated label that indicates the number of remaining guesses
    private Path        workFile;
    private StackPane[]      possibleGuess; //reference to guesses
    private char[]      alphabet = "abcdefghijklmnopqrstuvwxyz".toCharArray();
    private StackPane[]   stackPane; //reference to rectangles for progress
    private Button      hint;
    private Workspace  gameWorkspace;

    public HangmanController(AppTemplate appTemplate, Button gameButton) {
        this(appTemplate);
        this.gameButton = gameButton;
    }

    public HangmanController(AppTemplate appTemplate) {
        this.appTemplate = appTemplate;
        this.gamestate = GameState.UNINITIALIZED;
    }

    public void enableGameButton() {
        if (gameButton == null) {
            Workspace workspace = (Workspace) appTemplate.getWorkspaceComponent();
            gameButton = workspace.getStartGame();
        }
        gameButton.setDisable(false);
    }

    public void disableGameButton() {
        if (gameButton == null) {
            Workspace workspace = (Workspace) appTemplate.getWorkspaceComponent();
            gameButton = workspace.getStartGame();
        }
        gameButton.setDisable(true);
    }

    public void disableHintButton(){
        hint.setDisable(true   );
    }

    public void setGameState(GameState gamestate) {
        this.gamestate = gamestate;
    }

    public GameState getGamestate() {
        return this.gamestate;
    }

    /**
     * In the homework code given to you, we had the line
     * gamedata = new GameData(appTemplate, true);
     * This meant that the 'gamedata' variable had access to the app, but the data component of the app was still
     * the empty game data! What we need is to change this so that our 'gamedata' refers to the data component of
     * the app, instead of being a new object of type GameData. There are several ways of doing this. One of which
     * is to write (and use) the GameData#init() method.
     */
    public void start() { //starting new game
        gamedata = (GameData) appTemplate.getDataComponent(); //create data
        success = false;
        discovered = 0;

        gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent(); //creating workspace

        gamedata.init();
        setGameState(GameState.INITIALIZED_UNMODIFIED); //identifying first type of game

        HBox remainingGuessBox = gameWorkspace.getRemainingGuessBox();
        HBox guessedBox = gameWorkspace.getGuesses();
        HBox guessedLetters    = (HBox) gameWorkspace.getGameTextsPane().getChildren().get(1);
        HBox hintArea = (HBox) gameWorkspace.getGameTextsPane().getChildren().get(3);

        hint = new Button("Hint");
        hintArea.getChildren().add(hint);
        hint.setVisible(true);

        if(isDifficultWord(gamedata.getTargetWord())){ //check if difficult word
            hint.setVisible(true);
        }


        remains = new Label(Integer.toString(GameData.TOTAL_NUMBER_OF_GUESSES_ALLOWED)); //number next to reamining guesses
        remainingGuessBox.getChildren().addAll(new Label("Remaining Guesses: "), remains);

        initWordGraphics(guessedLetters);
        initGuessedGraphics(guessedBox);
        play();
    }

    public boolean isDifficultWord(String targetWord){
        int uniqueLetter = 0;
        boolean[] alphabetCheck = new boolean[26];
        for(int i = 0; i < targetWord.length(); i++){
            if(alphabetCheck[targetWord.charAt(i) - 97] != true) {
                alphabetCheck[targetWord.charAt(i) - 97] = true;
                uniqueLetter++;
            }
        }
        if(uniqueLetter > 7)
            return true;
        return false;
    }

    private void end() {
        appTemplate.getGUI().getPrimaryScene().setOnKeyTyped(null);
        gameButton.setDisable(true);
        setGameState(GameState.ENDED);
        appTemplate.getGUI().updateWorkspaceToolbar(gamestate.equals(GameState.INITIALIZED_MODIFIED));
        Platform.runLater(() -> {
            PropertyManager           manager    = PropertyManager.getManager();
            AppMessageDialogSingleton dialog     = AppMessageDialogSingleton.getSingleton();
            String                    endMessage = manager.getPropertyValue(success ? GAME_WON_MESSAGE : GAME_LOST_MESSAGE);
            if (!success) {
//                endMessage += String.format(" (the word was \"%s\")", gamedata.getTargetWord()); //pop up saying  word

                for(int i = 0; i< gamedata.getTargetWord().length(); i++){
                    if(stackPane[i].getChildren().get(1).isVisible() == false) { //if letter is not visible
                        stackPane[i].getChildren().get(1).setVisible(true);
                        ((Rectangle)stackPane[i].getChildren().get(0)).setFill(Color.RED);
                    }
                }
            }
            if (dialog.isShowing())
                dialog.toFront();
            else
                dialog.show(manager.getPropertyValue(GAME_OVER_TITLE), endMessage);
        });
    }

    private void initGuessedGraphics(HBox guessed){ // needs to be updated on bad guesses
        possibleGuess = new StackPane[alphabet.length];
        for (int i = 0; i < alphabet.length; i++){ //traverse through all bad guess array
            possibleGuess[i] = addAlphabetRectangles(Character.toString(alphabet[i]));
        }

        if(gamedata.getGoodGuesses().toArray().length != 0 || gamedata.getBadGuesses().toArray().length != 10){
            char letter;
            int letterPos;
            //go through good guesses
            for(int i = 0; i<gamedata.getGoodGuesses().toArray().length; i++){
                letter = ((char)gamedata.getGoodGuesses().toArray()[i]);
                letterPos = letter - 97;

                ((Rectangle)possibleGuess[letterPos].getChildren().get(0)).setFill(Color.YELLOWGREEN);

            }
            //go through bad guesses
            for (int i = 0; i<gamedata.getBadGuesses().toArray().length; i++){
                letter = (char)gamedata.getBadGuesses().toArray()[i];
                letterPos = letter - 97;
                ((Rectangle)possibleGuess[letterPos].getChildren().get(0)).setFill(Color.YELLOWGREEN);
            }
        }
        guessed.getChildren().addAll(possibleGuess);

    }

    private void initWordGraphics(HBox guessedLetters) { //change method to add letters in white boxes
        char[] targetword = gamedata.getTargetWord().toCharArray(); //changes target word into text array
        progress = new Text[targetword.length];
        stackPane = new StackPane[targetword.length]; // stack pane array for rects
        String letter;
        for (int i = 0; i < stackPane.length; i++) { //creates words under HBox
            letter = Character.toString(targetword[i]);
            progress[i] = new Text(letter); // writes entire word, using text class to display word
            progress[i].setVisible(false); //makes all the letters invisible
            stackPane[i] = addRectangles(letter);
        }

        guessedLetters.getChildren().addAll(stackPane);
    }

    private StackPane addRectangles(String letter){
        StackPane stack = new StackPane();
        Rectangle rect = new Rectangle(20,20);
        rect.setFill(Color.BLACK);

        Text text = new Text(letter.toUpperCase());
        text.setFill(Color.WHITE);

        stack.getChildren().addAll(rect,text); //adding things to the stack pane


        stack.setAlignment(Pos.CENTER_LEFT);
        stack.setMargin(rect, new Insets(0, 10, 0, 0));
        stack.getChildren().get(1).setVisible(false); //make letters invisible

//        guessedBox.getChildren().add(stack);
        return stack;
    }

    private StackPane addAlphabetRectangles(String letter){
        StackPane stack = new StackPane();
        Rectangle rect = new Rectangle(20,20);
        rect.setFill(Color.DARKGREEN);

        Text text = new Text(letter.toUpperCase());
        text.setFill(Color.WHITE);

        stack.getChildren().addAll(rect,text); //adding things to the stack pane


        stack.setAlignment(Pos.CENTER_LEFT);
        stack.setMargin(rect, new Insets(0, 10, 0, 0));

        return stack;
    }

    public void play() {
        disableGameButton();
        AnimationTimer timer = new AnimationTimer() {
            @Override
            public void handle(long now) {
                appTemplate.getGUI().updateWorkspaceToolbar(gamestate.equals(GameState.INITIALIZED_MODIFIED));
//                AppMessageDialogSingleton invalidLetter = new AppMessageDialogSingleton();

                hint.setOnMouseClicked(e -> showHint());
                success = (discovered == progress.length);
                if (success)
                    stop();

                appTemplate.getGUI().getPrimaryScene().setOnKeyTyped((KeyEvent event) -> {
                    char guess = event.getCharacter().charAt(0);
                    guess = Character.toLowerCase(guess);
                    int letterPos = guess - 97; //find position in possibleGuess array


                    if(Character.isLetter(guess)){
                        if (!alreadyGuessed(guess)) {
                            boolean goodguess = false;
                            ((Rectangle)possibleGuess[letterPos].getChildren().get(0)).setFill(Color.YELLOWGREEN);
                            for (int i = 0; i < progress.length; i++) {
                                if (gamedata.getTargetWord().charAt(i) == guess) { // looks through entire word
                                    stackPane[i].getChildren().get(1).setVisible(true);
                                    gamedata.addGoodGuess(guess);
                                    goodguess = true;
                                    progress[i].setText("0");
                                    discovered++;
                                }
                            }
                            if (!goodguess) {
                                gamedata.addBadGuess(guess);// show bad guess
                                gameWorkspace.setVisHangman(9 - gamedata.getRemainingGuesses());
                                if(gamedata.getRemainingGuesses() == 1){
                                    disableHintButton();
                                }
                                ((Rectangle)possibleGuess[letterPos].getChildren().get(0)).setFill(Color.YELLOWGREEN);
                            }

                            success = (discovered == progress.length);
                            remains.setText(Integer.toString(gamedata.getRemainingGuesses()));
                        }
                        setGameState(GameState.INITIALIZED_MODIFIED);
                    }

                });
                if (gamedata.getRemainingGuesses() <= 0 || success)
                    stop();
            }


            @Override
            public void stop() {
                super.stop();
                end();
            }
        };
        timer.start();
    }

    public void showHint(){
        int unknownsCount = 0;
        boolean[] alphabetCheck = new boolean[25];
        //get all unique unknown letters with no duplicates
        for(int i = 0; i< stackPane.length; i++){
            int letterPos = (progress[i].getText().charAt(0)-97);
            if(!(progress[i].getText().equalsIgnoreCase("0")) && alphabetCheck[letterPos] != true) { //undiscovered letter
                alphabetCheck[letterPos] = true; //not sure if toString right
                unknownsCount++;
            }
        }

        char[] unknownsArray = new char[unknownsCount];
        int arrayPos = 0;

        //put unknown letters into an array
        for(int i = 0; i < alphabetCheck.length; i++){
            if(alphabetCheck[i]) {
                unknownsArray[arrayPos] = alphabet[i];
                arrayPos++;
            }
        }

        int randomChar = (int) Math.random()*(arrayPos+1); //get random letter in the unknownsArray
        char hintedLetter = Character.toUpperCase(unknownsArray[randomChar]);

        //shows hinted letters
        for(int i = 0; i<stackPane.length; i++){
            if(((Text)stackPane[i].getChildren().get(1)).getText().charAt(0) == (hintedLetter)){
                stackPane[i].getChildren().get(1).setVisible(true);
                ((Rectangle) stackPane[i].getChildren().get(0)).setFill(Color.BLUE);

                gamedata.addGoodGuess(hintedLetter);
                discovered++;

            }
        }

        //updates guessed array
        for (int i = 0; i<possibleGuess.length; i++){
            if(((Text)possibleGuess[i].getChildren().get(1)).getText().charAt(0) == (hintedLetter)){
                ((Rectangle) possibleGuess[i].getChildren().get(0)).setFill(Color.YELLOWGREEN);
            }
        }

        gamedata.decrementRemainingGuesses();
        gameWorkspace.setVisHangman(9 - gamedata.getRemainingGuesses());
        remains.setText(Integer.toString(gamedata.getRemainingGuesses()));




        disableHintButton();
    }

    private void restoreGUI() {
        disableGameButton();
        gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
        gameWorkspace.reinitialize();

        HBox guessedBox    = (HBox) gameWorkspace.getGameTextsPane().getChildren().get(1);
        restoreWordGraphics(guessedBox); //shows correct guesses. target word

        HBox remainingGuessBox = gameWorkspace.getRemainingGuessBox();
        remains = new Label(Integer.toString(gamedata.getRemainingGuesses()));
        remainingGuessBox.getChildren().addAll(new Label("Remaining Guesses: "), remains);

        HBox guessedLetters = gameWorkspace.getGuesses();
        restoreGuessedGraphics(guessedLetters);

        //show hangman
        for(int i = 0; i< (10 - gamedata.getRemainingGuesses()); i++){
            gameWorkspace.setVisHangman(i);
        }



        success = false;
        play();
    }

    private void restoreWordGraphics(HBox guessedWord) {
        discovered = 0;
        char[] targetword = gamedata.getTargetWord().toCharArray();
        progress = new Text[targetword.length];
        stackPane = new StackPane[targetword.length]; // stack pane array for rects
        String letter;
        for (int i = 0; i < progress.length; i++) {
            letter = Character.toString(targetword[i]);
            progress[i] = new Text(letter); //shows word
            progress[i].setVisible(true); //makes all the letters invisible
            stackPane[i] = addRectangles(letter);

            stackPane[i].getChildren().get(1).setVisible(gamedata.getGoodGuesses().contains(progress[i].getText().charAt(0)));

            if (stackPane[i].getChildren().get(1).isVisible()) //dot
                discovered++;
        }
//        initGuessedGraphics(gu);
        guessedWord.getChildren().addAll(stackPane);

    }

    private void restoreGuessedGraphics(HBox guessedLetters){
        initGuessedGraphics(guessedLetters);



    }


//    private void restoreGuessedGraphics(){
//        possibleGuess = new Text[alphabet.length];
//        for (int i = 0; i < possibleGuess.length; i++){
//            possibleGuess[i] = new Text(Character.toString(alphabet[i]) + " ");
//            possibleGuess[i].setVisible(gamedata.getGoodGuesses().contains(possibleGuess[i].getText().charAt(0)));
//            possibleGuess[i].setVisible(gamedata.getBadGuesses().contains(possibleGuess[i].getText().charAt(0)));
//        }
//    }

    private boolean alreadyGuessed(char c) {
        return gamedata.getGoodGuesses().contains(c) || gamedata.getBadGuesses().contains(c);
    }

    @Override
    public void handleNewRequest() {
        AppMessageDialogSingleton messageDialog   = AppMessageDialogSingleton.getSingleton();
        PropertyManager           propertyManager = PropertyManager.getManager();
        boolean                   makenew         = true;
        if (gamestate.equals(GameState.INITIALIZED_MODIFIED))
            try {
                makenew = promptToSave();
            } catch (IOException e) {
                messageDialog.show(propertyManager.getPropertyValue(NEW_ERROR_TITLE), propertyManager.getPropertyValue(NEW_ERROR_MESSAGE));
            }
        if (makenew) {
            appTemplate.getDataComponent().reset();                // reset the data (should be reflected in GUI)
            appTemplate.getWorkspaceComponent().reloadWorkspace(); // load data into workspace
            ensureActivatedWorkspace();                            // ensure workspace is activated
            workFile = null;                                       // new workspace has never been saved to a file
            ((Workspace) appTemplate.getWorkspaceComponent()).reinitialize();
            enableGameButton();
        }
        if (gamestate.equals(GameState.ENDED)) {
            appTemplate.getGUI().updateWorkspaceToolbar(false);
            Workspace gameWorkspace = (Workspace) appTemplate.getWorkspaceComponent();
            gameWorkspace.reinitialize();
        }

    }

    @Override
    public void handleSaveRequest() throws IOException {
        PropertyManager propertyManager = PropertyManager.getManager();
        if (workFile == null) {
            FileChooser filechooser = new FileChooser();
            Path        appDirPath  = Paths.get(propertyManager.getPropertyValue(APP_TITLE)).toAbsolutePath();
            Path        targetPath  = appDirPath.resolve(APP_WORKDIR_PATH.getParameter());
            filechooser.setInitialDirectory(targetPath.toFile());
            filechooser.setTitle(propertyManager.getPropertyValue(SAVE_WORK_TITLE));
            String description = propertyManager.getPropertyValue(WORK_FILE_EXT_DESC);
            String extension   = propertyManager.getPropertyValue(WORK_FILE_EXT);
            ExtensionFilter extFilter = new ExtensionFilter(String.format("%s (*.%s)", description, extension),
                                                            String.format("*.%s", extension));
            filechooser.getExtensionFilters().add(extFilter);
            File selectedFile = filechooser.showSaveDialog(appTemplate.getGUI().getWindow());
            if (selectedFile != null)
                save(selectedFile.toPath());
        } else
            save(workFile);
    }

    @Override
    public void handleLoadRequest() throws IOException {
        boolean load = true;
        if (gamestate.equals(GameState.INITIALIZED_MODIFIED))
            load = promptToSave();
        if (load) {
            PropertyManager propertyManager = PropertyManager.getManager();
            FileChooser     filechooser     = new FileChooser();
            Path            appDirPath      = Paths.get(propertyManager.getPropertyValue(APP_TITLE)).toAbsolutePath();
            Path            targetPath      = appDirPath.resolve(APP_WORKDIR_PATH.getParameter());
            filechooser.setInitialDirectory(targetPath.toFile());
            filechooser.setTitle(propertyManager.getPropertyValue(LOAD_WORK_TITLE));
            String description = propertyManager.getPropertyValue(WORK_FILE_EXT_DESC);
            String extension   = propertyManager.getPropertyValue(WORK_FILE_EXT);
            ExtensionFilter extFilter = new ExtensionFilter(String.format("%s (*.%s)", description, extension),
                                                            String.format("*.%s", extension));
            filechooser.getExtensionFilters().add(extFilter);
            File selectedFile = filechooser.showOpenDialog(appTemplate.getGUI().getWindow());
            if (selectedFile != null && selectedFile.exists()) {
                load(selectedFile.toPath());
                restoreGUI(); // restores the GUI to reflect the state in which the loaded game was last saved
            }
        }
    }

    @Override
    public void handleExitRequest() {
        try {
            boolean exit = true;
            if (gamestate.equals(GameState.INITIALIZED_MODIFIED))
                exit = promptToSave();
            if (exit)
                System.exit(0);
        } catch (IOException ioe) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            PropertyManager           props  = PropertyManager.getManager();
            dialog.show(props.getPropertyValue(SAVE_ERROR_TITLE), props.getPropertyValue(SAVE_ERROR_MESSAGE));
        }
    }

    private void ensureActivatedWorkspace() {
        appTemplate.getWorkspaceComponent().activateWorkspace(appTemplate.getGUI().getAppPane());
    }

    private boolean promptToSave() throws IOException {
        PropertyManager            propertyManager   = PropertyManager.getManager();
        YesNoCancelDialogSingleton yesNoCancelDialog = YesNoCancelDialogSingleton.getSingleton();

        yesNoCancelDialog.show(propertyManager.getPropertyValue(SAVE_UNSAVED_WORK_TITLE),
                               propertyManager.getPropertyValue(SAVE_UNSAVED_WORK_MESSAGE));

        if (yesNoCancelDialog.getSelection().equals(YesNoCancelDialogSingleton.YES))
            handleSaveRequest();

        return !yesNoCancelDialog.getSelection().equals(YesNoCancelDialogSingleton.CANCEL);
    }

    /**
     * A helper method to save work. It saves the work, marks the current work file as saved, notifies the user, and
     * updates the appropriate controls in the user interface
     *
     * @param target The file to which the work will be saved.
     * @throws IOException
     */
    private void save(Path target) throws IOException {
        appTemplate.getFileComponent().saveData(appTemplate.getDataComponent(), target);
        workFile = target;
        setGameState(GameState.INITIALIZED_UNMODIFIED);
        AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
        PropertyManager           props  = PropertyManager.getManager();
        dialog.show(props.getPropertyValue(SAVE_COMPLETED_TITLE), props.getPropertyValue(SAVE_COMPLETED_MESSAGE));
    }

    /**
     * A helper method to load saved game data. It loads the game data, notified the user, and then updates the GUI to
     * reflect the correct state of the game.
     *
     * @param source The source data file from which the game is loaded.
     * @throws IOException
     */
    private void load(Path source) throws IOException {
        // load game data
        appTemplate.getFileComponent().loadData(appTemplate.getDataComponent(), source);

        // set the work file as the file from which the game was loaded
        workFile = source;

        // notify the user that load was successful
        AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
        PropertyManager           props  = PropertyManager.getManager();
        dialog.show(props.getPropertyValue(LOAD_COMPLETED_TITLE), props.getPropertyValue(LOAD_COMPLETED_MESSAGE));

        setGameState(GameState.INITIALIZED_UNMODIFIED);
        Workspace gameworkspace = (Workspace) appTemplate.getWorkspaceComponent();
        ensureActivatedWorkspace();
        gameworkspace.reinitialize();
        gamedata = (GameData) appTemplate.getDataComponent();
    }
}
